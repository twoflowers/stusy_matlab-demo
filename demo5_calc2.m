% 自定义函数 example2
% 有多个输出，写在[]中；输出只有一个时[]可以省略
% 函数结尾的result可以写，但没必要（运行到最后一行函数自然停止）
% 自定义函数接受不固定个输入参数，请搜索 “选择性参数、inputParser”
function [y1,y2] = demo5_calc2(x1,x2)
y1 = x1^2;
y2 = x2^2;

disp(['max(x1,x2):',num2str(max(x1,x2))]);

% ******************************************
% max()只能被同一文件中的其它函数调用，是子函数
% 私有函数有 private 声明的子函数，它们只能被父目录中的函数访问
% 子函数和私有函数主要用于限制 MATLAB 函数的访问
function a = max(m,n)

if(m<n)
    a = n;
else if(m > n)
     a = m;
    end;
end;



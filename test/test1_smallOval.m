%  数学建模公式测试 ---- 小椭圆模型
% 
% a：长半轴 mm
% b：短半轴 mm
% L：罐体长 mm
% h：液位高 mm
% H：罐体高 mm
% volumnOval: L
% 
function volumnOval = test1_smallOval()

a = 1.78/2*1000;
b = 1.2/2*1000;
L = 2.45*1000;
H = 1.2*1000;
h = H;

v1 = 0.5 * pi* a * b + a / b * (h - b) * (sqrt(2 * b * h - h*h));
v2 = a * b * asin((h - b) / b);

volumnOval = (v1 + v2) * L/10^6 ; % L 



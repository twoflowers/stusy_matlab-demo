% 计算 area 的值，并在 Command Window 中打印出来
radius = 2.5;
area = pi*radius^2;
string = ['the area of the circle is ',num2str(area)];
disp(string);

% 画出 y = sin(x) 的图像
% x = [0:0.1:6] 注意是 ： 而不是 ，
x = 0:0.1:6;
y = sin(x);
plot(x,y);
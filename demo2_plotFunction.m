    % plot() 的使用方法
    x=0:1:10;
    y=x.^2-10*x+15;
    % plot(x,y);
    % 曲线为红色的虚线,重要的数值用蓝色的小圆圈表示
    % plot(x,y,'r--',x,y,'bo');
    title ('Plot of y=x.^2-10*x+15');
    xlabel ('x');
    ylabel ('y');
    grid on; % grid off 去除网格线

    % 联合绘图
    x1 = 0:pi/100:2*pi;
    y1 = sin(2*x1);
    y2 = 2*cos(2*x1);
    plot (x1,y1,x1,y2);
    title(' Plot of f(x)=sin(2x) and its derivative');
    xlabel('x');
    ylabel('y');
    % legend 制作图例
    legend('f(x)','d/dx f(x)');
    grid on;
    % 对数尺度
    semilogx(x1,y1)

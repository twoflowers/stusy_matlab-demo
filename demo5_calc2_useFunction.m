% 使用自定义函数 example2
x1 = input('input x1:');
x2 = input('input x2:');
% 调用自定义函数格式就是：
%   自定义函数文件中第一行代码去掉 function 剩下的部分
%       function [y1,y2] = demo5_calc2(x1,x2)
[y1,y2] = demo5_calc2(x1,x2);
disp(['y1 is ', num2str(y1)]);
disp(['y2 is ', num2str(y2)]);
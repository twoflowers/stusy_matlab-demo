% C 语言中，变量类型和变量在使用之前必须强制声明。这种语言叫强类型语言。
% MATLAB 是弱类型语言，直接赋值就可以创建变量，变量类型取决于创建时的类型。

var = 40*2i; % matlab 中i、j用来表示虚数，用 2i这种形式提高速度和鲁棒性
var2 = var/5;
array = [1,2,3,4]; % 数组中的元素可以用空格分隔，也可以用逗号分隔
x = 5;
y = 10;

% first：incr：last  克隆运算符，incr=1时可省略
arr1 = 1:2:10;
arr2 = 1:1:10;
arr3 = 1:10;

% 转置运算符
f = [1:4]';
g = 1:4;
h = [g' g'];

% 用input初始化变量
my_val = input('Enter an input value:');
my_val2 = input('Enter an input value:','s'); %input('descript:','s')输入的数据就被当字符串

% 输出显示格式 format short(默认)，format bank(无科学记数法)
aa = 4565.13246;
% fprintf()  和c语言中print用法一样。缺点：只能显示复数的实部,所以有虚数时要用disp
fprintf('The value of pi is %6.2f \n',pi);
% disp()  num2ster()   int2str
str = ['the value of pi='  num2str(pi)];
disp(str);


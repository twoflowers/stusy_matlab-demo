%   数学建模公式测试 ---- 实际油罐模型
% 
% a：长半轴 mm
% b：短半轴 mm
% L：罐体长 mm
% h：液位高 mm
% H：罐体高 mm
% 
function volumnOval = test3_tank()

a = 1.78/2*1000;
b = 1.2/2*1000;
L = 2.45*1000;
h = 1.2*1000;
H = 1.2*1000;

volumnOval = a+b+L+h+H;


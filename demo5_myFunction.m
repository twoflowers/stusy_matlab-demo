% 在这个文件里自定义函数，函数名必须和文件名一致
% 函数脚本不需要运行，只要保存即可
% function result = filename(params)  //这里的函数名字必须和文件名一致
% function block

function distance = demo5_myFunction (x1, y1, x2, y2)
%DIST2 Calculate the distance between two point
% Function DIST2 calculates the distance between
% two points (x1, y1) and (x2,y2) in a cartesian
% coordinate system.
%
% Calling sequence:
% res = dist2(x1, y1, x2, y2)
%
% Define variables:
% x1  --x-position of point 1
% y1  --y-position of point 1
% x2  --x-position of point 2
% y2  --y-position of point 2
% distance --Distance between points
%
% Record of revisions:
%  Date Pragrammer  Description of change
%  ========  ========== ================
%  12/15/98 S.J.Chapman Original code
%
% Calculate distance.

% 函数体内容
distance = sqrt((x2-x1).^2 + (y2-y1).^2);
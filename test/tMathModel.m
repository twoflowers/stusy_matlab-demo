% 调用自定义函数 mathModel
% 小椭圆模型体积
volumn = test1_smallOval();
fprintf('小椭圆模型体积是： %4.2f \n',volumn)

% 小椭圆倾斜体积
volumn = test2_smallOvalTilt();
fprintf('小椭圆倾斜体积是： %4.2f \n',volumn)

% 实际油罐体积
% volumn = test3_tank();
% fprintf('实际油罐体积是： %4.2f \n',volumn)

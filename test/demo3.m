syms x % 定义自变量
a = 1.78/2;% 长半轴
b = 1.2/2;% 短半轴
h = 1.2;% 液体高度

f = 2*a*sqrt(1-(x^2/b^2));  % 定义积分函数 y
% 数学建模积分式
g = int(f,x,-b,h-b);  % 定义原函数的积分式
gg = (267*pi)/500;
% 数学建模化简结果
S = a*b*asin((h-b)/b)+a*b*pi/2+a*b/2*sin(2*asin((h-b)/b));
% disp(g)
% disp(gg)
disp(S)

% google 搜索的计算公式
% https://arachnoid.com/TankCalc/index.html
R = 0.6;
y=1.2;
Sc = sqrt((2*R-y)*y) * (y-R) + R^2*acos(1-y/R);
disp(Sc)
